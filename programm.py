from size import convert_SI

try:
    value = int(input("Введите значение "))
except Exception:
    print("Ошибка числа")
    exit(1)
unit_in = str(input("Введите изначальную шкалу "))
unit_out = str(input("Введите конечную шкалу "))
print(convert_SI(value, unit_in, unit_out))
